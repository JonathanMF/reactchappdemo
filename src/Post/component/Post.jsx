import React from 'react';

const Post = (props) => (
	<div className="alert alert-info" role="alert">
	 <strong>{ props.userName } : </strong>

        { props.postBody }
	</div>
)

export default Post;